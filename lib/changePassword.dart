import 'package:flutter/material.dart';
import 'package:portal_app/model/ChangePasswordRequest.dart';
import 'package:portal_app/service/api_services.dart';

import 'local/SharedPreferencesHelper.dart';
import 'login.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => new _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  int uaaUserId = 0;

  @override
  void initState() {
    super.initState();
    SharedPreferencesHelper.getUserId().then((id) {
      setState(() {
        uaaUserId = id;
      });
    });
  }

  final _oldPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final worklogHeader = new Container(
    decoration: BoxDecoration(
      color: Color.fromRGBO(240, 103, 38, 1.0),
    ),
    alignment: Alignment.center,
    height: 58.0,
    child: new Text(
      'Change Password',
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 103, 38, 1.0),
        title: Container(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/sunflowerLogoWhite.png',
            fit: BoxFit.scaleDown,
          ),
        ),
        leading: IconButton(
          icon: Image.asset(
            'assets/menu.png',
            fit: BoxFit.contain,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
              icon: Image.asset(
                'assets/cancel.png',
                fit: BoxFit.contain,
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
//                  alignment: Alignment.center,
                  child: Image.asset(
                'assets/sunflower_logo.png',
                fit: BoxFit.contain,
              )),
              decoration: BoxDecoration(color: Colors.white12),
            ),
            ListTile(
              title: Text('APP DETAILS'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('HOLIDAY LIST'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('CHANGE PASSWORD'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('LOGOUT'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            worklogHeader,
            Expanded(
                child: SafeArea(
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                children: <Widget>[
                  SizedBox(height: 30.0),
                  TextField(
                    controller: _oldPasswordController,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      filled: false,
                      labelText: 'Old Password',
                    ),
                  ),
                  SizedBox(height: 12.0),
                  TextField(
                    controller: _newPasswordController,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      filled: false,
                      labelText: 'New Password',
                    ),
                    obscureText: true,
                  ),
                  SizedBox(height: 12.0),
                  TextField(
                    controller: _confirmPasswordController,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      filled: false,
                      labelText: 'Confirm Password',
                    ),
                    obscureText: true,
                  ),
                  SizedBox(height: 24.0),
                  RaisedButton(
                    child: Text('Save',
                        style: TextStyle(color: Colors.white, height: 1.5)),
                    color: Colors.deepOrange,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(26.0),
                    ),
                    padding: EdgeInsets.all(12),
                    onPressed: () {
                      if (_oldPasswordController.text.length > 0 &&
                          _newPasswordController.text.length > 0 &&
                          _confirmPasswordController.text.length > 0) {
                        if (_newPasswordController.text ==
                            _confirmPasswordController.text) {
                          changePassword(new ChangePasswordRequest(
                                  newPassword: _newPasswordController.text,
                                  oldPassword: _oldPasswordController.text,
                                  confirmPassword: _confirmPasswordController.text,
                                  userId: uaaUserId))
                              .then((resp) {
                            if (resp) {
                              Navigator.of(context).pop();
                              SharedPreferencesHelper.clear();
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(builder: (context) => LoginPage()),
                                      (Route<dynamic> route) => false);
                            } else {
                              _scaffoldKey.currentState
                                  .showSnackBar(new SnackBar(
                                duration: new Duration(seconds: 4),
                                content: new Row(
                                  children: <Widget>[
                                    new Text(
                                        "something went wrong.. Please try again later!")
                                  ],
                                ),
                              ));
                            }
                          });
                        } else {
                          _scaffoldKey.currentState.showSnackBar(new SnackBar(
                            duration: new Duration(seconds: 4),
                            content: new Row(
                              children: <Widget>[
                                new Text(
                                    "New password and confirm password not match!!")
                              ],
                            ),
                          ));
                        }
                      } else {
                        _scaffoldKey.currentState.showSnackBar(new SnackBar(
                          duration: new Duration(seconds: 4),
                          content: new Row(
                            children: <Widget>[
                              new Text("Please enter valid information")
                            ],
                          ),
                        ));
                      }
                    },
                  ),
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}
