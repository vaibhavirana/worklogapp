// To parse this JSON data, do
//
//     final holidayResponse = holidayResponseFromJson(jsonString);

import 'dart:convert';

List<HolidayResponse> holidayResponseFromJson(String str) => List<HolidayResponse>.from(json.decode(str).map((x) => HolidayResponse.fromJson(x)));

String holidayResponseToJson(List<HolidayResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HolidayResponse {
  int id;
  String holidayName;
  DateTime startDate;
  DateTime endDate;

  HolidayResponse({
    this.id,
    this.holidayName,
    this.startDate,
    this.endDate,
  });

  factory HolidayResponse.fromJson(Map<String, dynamic> json) => HolidayResponse(
    id: json["id"],
    holidayName: json["holidayName"],
    startDate: DateTime.parse(json["startDate"]),
    endDate: DateTime.parse(json["endDate"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "holidayName": holidayName,
    "startDate": startDate.toIso8601String(),
    "endDate": endDate.toIso8601String(),
  };
}
