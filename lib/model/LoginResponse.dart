import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  String accessToken;
  int expiresIn;
  int iat;
  String jti;
  String refreshToken;
  String scope;
  String tokenType;

  LoginResponse({
    this.accessToken,
    this.expiresIn,
    this.iat,
    this.jti,
    this.refreshToken,
    this.scope,
    this.tokenType,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    accessToken: json["access_token"],
    expiresIn: json["expires_in"],
    iat: json["iat"],
    jti: json["jti"],
    refreshToken: json["refresh_token"],
    scope: json["scope"],
    tokenType: json["token_type"],
  );

  Map<String, dynamic> toJson() => {
    "access_token": accessToken,
    "expires_in": expiresIn,
    "iat": iat,
    "jti": jti,
    "refresh_token": refreshToken,
    "scope": scope,
    "token_type": tokenType,
  };
}