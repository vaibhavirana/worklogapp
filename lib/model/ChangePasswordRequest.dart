// To parse this JSON data, do
//
//     final changePasswordRequest = changePasswordRequestFromJson(jsonString);

import 'dart:convert';

ChangePasswordRequest changePasswordRequestFromJson(String str) => ChangePasswordRequest.fromJson(json.decode(str));

String changePasswordRequestToJson(ChangePasswordRequest data) => json.encode(data.toJson());

class ChangePasswordRequest {
  String newPassword;
  String oldPassword;
  String confirmPassword;
  int userId;

  ChangePasswordRequest({
    this.newPassword,
    this.oldPassword,
    this.confirmPassword,
    this.userId,
  });

  factory ChangePasswordRequest.fromJson(Map<String, dynamic> json) => ChangePasswordRequest(
    newPassword: json["newPassword"],
    oldPassword: json["oldPassword"],
    confirmPassword: json["confirmPassword"],
    userId: json["userId"],
  );

  Map<String, dynamic> toJson() => {
    "newPassword": newPassword,
    "oldPassword": oldPassword,
    "confirmPassword": confirmPassword,
    "userId": userId,
  };
}
