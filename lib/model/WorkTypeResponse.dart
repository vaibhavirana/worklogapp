// To parse this JSON data, do
//
//     final workTypeResponse = workTypeResponseFromJson(jsonString);

import 'dart:convert';

List<WorkTypeResponse> workTypeResponseFromJson(String str) => List<WorkTypeResponse>.from(json.decode(str).map((x) => WorkTypeResponse.fromJson(x)));

String workTypeResponseToJson(List<WorkTypeResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class WorkTypeResponse {
  int id;
  String worktypes;

  WorkTypeResponse({
    this.id,
    this.worktypes,
  });

  factory WorkTypeResponse.fromJson(Map<String, dynamic> json) => WorkTypeResponse(
    id: json["id"],
    worktypes: json["worktypes"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "worktypes": worktypes,
  };
}
