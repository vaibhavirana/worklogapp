// To parse this JSON data, do
//
//     final addWorklogRequest = addWorklogRequestFromJson(jsonString);

import 'dart:convert';

AddWorklogRequest addWorklogRequestFromJson(String str) => AddWorklogRequest.fromJson(json.decode(str));

String addWorklogRequestToJson(AddWorklogRequest data) => json.encode(data.toJson());

class AddWorklogRequest {
  int projectId;
  int worktypeId;
  String date;
  int hours;
  String note;
  int uaaUserId;

  AddWorklogRequest({
    this.projectId,
    this.worktypeId,
    this.date,
    this.hours,
    this.note,
    this.uaaUserId,
  });

  factory AddWorklogRequest.fromJson(Map<String, dynamic> json) => AddWorklogRequest(
    projectId: json["projectId"],
    worktypeId: json["worktypeId"],
    date: (json["date"]),
    hours: json["hours"],
    note: json["note"],
    uaaUserId: json["uaaUserId"],
  );

  Map<String, dynamic> toJson() => {
    "projectId": projectId,
    "worktypeId": worktypeId,
    "date": date,
    "hours": hours,
    "note": note,
    "uaaUserId": uaaUserId,
  };

  @override
  String toString() {
    return 'AddWorklogRequest{projectId: $projectId, worktypeId: $worktypeId, date: $date, hours: $hours, note: $note, uaaUserId: $uaaUserId}';
  }


}
