// To parse this JSON data, do
//
//     final loginFailureResponse = loginFailureResponseFromJson(jsonString);

import 'dart:convert';

LoginFailureResponse loginFailureResponseFromJson(String str) => LoginFailureResponse.fromJson(json.decode(str));

String loginFailureResponseToJson(LoginFailureResponse data) => json.encode(data.toJson());

class LoginFailureResponse {
  String type;
  String title;
  int status;

  LoginFailureResponse({
    this.type,
    this.title,
    this.status,
  });

  factory LoginFailureResponse.fromJson(Map<String, dynamic> json) => LoginFailureResponse(
    type: json["type"],
    title: json["title"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "title": title,
    "status": status,
  };
}
