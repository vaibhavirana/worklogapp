// To parse this JSON data, do
//
//     final employeeRequest = employeeRequestFromJson(jsonString);

import 'dart:convert';

EmployeeRequest employeeRequestFromJson(String str) => EmployeeRequest.fromJson(json.decode(str));

String employeeRequestToJson(EmployeeRequest data) => json.encode(data.toJson());

class EmployeeRequest {
  String email;
  String userName;

  EmployeeRequest({
    this.email,
    this.userName,
  });

  factory EmployeeRequest.fromJson(Map<String, dynamic> json) => EmployeeRequest(
    email: json["email"],
    userName: json["userName"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "userName": userName,
  };

  @override
  String toString() {
    return 'EmployeeRequest{email: $email, userName: $userName}';
  }


}
