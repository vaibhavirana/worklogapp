// To parse this JSON data, do
//
//     final worklogRequest = worklogRequestFromJson(jsonString);

import 'dart:convert';

WorklogRequest worklogRequestFromJson(String str) => WorklogRequest.fromJson(json.decode(str));

String worklogRequestToJson(WorklogRequest data) => json.encode(data.toJson());

class WorklogRequest {
  DateTime startDate;
  DateTime endDate;
  String filterBy;
  int uaaUserId;
  String workType;
  String projectName;

  WorklogRequest({
    this.startDate,
    this.endDate,
    this.filterBy,
    this.uaaUserId,
    this.workType,
    this.projectName,
  });

  factory WorklogRequest.fromJson(Map<String, dynamic> json) => WorklogRequest(
    startDate: DateTime.parse(json["startDate"]),
    endDate: DateTime.parse(json["endDate"]),
    filterBy: json["filterBy"],
    uaaUserId: json["uaaUserId"],
    workType: json["workType"],
    projectName: json["projectName"],
  );

  Map<String, dynamic> toJson() => {
    "startDate": "${startDate.year.toString().padLeft(4, '0')}-${startDate.month.toString().padLeft(2, '0')}-${startDate.day.toString().padLeft(2, '0')}",
    "endDate": "${endDate.year.toString().padLeft(4, '0')}-${endDate.month.toString().padLeft(2, '0')}-${endDate.day.toString().padLeft(2, '0')}",
    "filterBy": filterBy,
    "uaaUserId": uaaUserId,
    "workType": workType,
    "projectName": projectName,
  };

  @override
  String toString() {
    return 'WorklogRequest{startDate: $startDate, endDate: $endDate, filterBy: $filterBy, uaaUserId: $uaaUserId, workType: $workType, projectName: $projectName}';
  }


}
