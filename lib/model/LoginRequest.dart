// To parse this JSON data, do
//
//     final loginRequest = loginRequestFromJson(jsonString);

import 'dart:convert';

LoginRequest loginRequestFromJson(String str) => LoginRequest.fromJson(json.decode(str));

String loginRequestToJson(LoginRequest data) => json.encode(data.toJson());

class LoginRequest {
  String username;
  String password;
  bool rememberMe;

  LoginRequest({
    this.username,
    this.password,
    this.rememberMe,
  });

  factory LoginRequest.fromJson(Map<String, dynamic> json) => LoginRequest(
    username: json["username"],
    password: json["password"],
    rememberMe: json["rememberMe"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "password": password,
    "rememberMe": rememberMe,
  };

  @override
  String toString() {
    return 'LoginRequest{username: $username, password: $password, rememberMe: $rememberMe}';
  }


}
