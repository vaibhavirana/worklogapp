// To parse this JSON data, do
//
//     final worklogResponse = worklogResponseFromJson(jsonString);

import 'dart:convert';

WorklogResponse worklogResponseFromJson(String str) => WorklogResponse.fromJson(json.decode(str));

String worklogResponseToJson(WorklogResponse data) => json.encode(data.toJson());

class WorklogResponse {
  List<Content> content;
  Pageable pageable;
  int totalElements;
  bool last;
  int totalPages;
  Sort sort;
  bool first;
  int numberOfElements;
  int size;
  int number;

  WorklogResponse({
    this.content,
    this.pageable,
    this.totalElements,
    this.last,
    this.totalPages,
    this.sort,
    this.first,
    this.numberOfElements,
    this.size,
    this.number,
  });

  factory WorklogResponse.fromJson(Map<String, dynamic> json) => WorklogResponse(
    content: List<Content>.from(json["content"].map((x) => Content.fromJson(x))),
    pageable: Pageable.fromJson(json["pageable"]),
    totalElements: json["totalElements"],
    last: json["last"],
    totalPages: json["totalPages"],
    sort: Sort.fromJson(json["sort"]),
    first: json["first"],
    numberOfElements: json["numberOfElements"],
    size: json["size"],
    number: json["number"],
  );

  Map<String, dynamic> toJson() => {
    "content": List<dynamic>.from(content.map((x) => x.toJson())),
    "pageable": pageable.toJson(),
    "totalElements": totalElements,
    "last": last,
    "totalPages": totalPages,
    "sort": sort.toJson(),
    "first": first,
    "numberOfElements": numberOfElements,
    "size": size,
    "number": number,
  };
}

class Content {
  int id;
  int hours;
  String note;
  String status;
  DateTime date;
  int projectId;
  String projectName;
  int worktypeId;
  String worktypeName;
  int uaaUserId;
  String userName;

  Content({
    this.id,
    this.hours,
    this.note,
    this.status,
    this.date,
    this.projectId,
    this.projectName,
    this.worktypeId,
    this.worktypeName,
    this.uaaUserId,
    this.userName,
  });

  factory Content.fromJson(Map<String, dynamic> json) => Content(
    id: json["id"],
    hours: json["hours"],
    note: json["note"],
    status: json["status"],
    date: DateTime.parse(json["date"]),
    projectId: json["projectId"],
    projectName: json["projectName"],
    worktypeId: json["worktypeId"],
    worktypeName: json["worktypeName"],
    uaaUserId: json["uaaUserId"],
    userName: json["userName"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "hours": hours,
    "note": note,
    "status": status,
    "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
    "projectId": projectId,
    "projectName": projectName,
    "worktypeId": worktypeId,
    "worktypeName": worktypeName,
    "uaaUserId": uaaUserId,
    "userName": userName,
  };
}

class Pageable {
  Sort sort;
  int pageSize;
  int pageNumber;
  int offset;
  bool unpaged;
  bool paged;

  Pageable({
    this.sort,
    this.pageSize,
    this.pageNumber,
    this.offset,
    this.unpaged,
    this.paged,
  });

  factory Pageable.fromJson(Map<String, dynamic> json) => Pageable(
    sort: Sort.fromJson(json["sort"]),
    pageSize: json["pageSize"],
    pageNumber: json["pageNumber"],
    offset: json["offset"],
    unpaged: json["unpaged"],
    paged: json["paged"],
  );

  Map<String, dynamic> toJson() => {
    "sort": sort.toJson(),
    "pageSize": pageSize,
    "pageNumber": pageNumber,
    "offset": offset,
    "unpaged": unpaged,
    "paged": paged,
  };
}

class Sort {
  bool sorted;
  bool unsorted;

  Sort({
    this.sorted,
    this.unsorted,
  });

  factory Sort.fromJson(Map<String, dynamic> json) => Sort(
    sorted: json["sorted"],
    unsorted: json["unsorted"],
  );

  Map<String, dynamic> toJson() => {
    "sorted": sorted,
    "unsorted": unsorted,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
