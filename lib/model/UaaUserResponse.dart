// To parse this JSON data, do
//
//     final uaaUserResponse = uaaUserResponseFromJson(jsonString);

import 'dart:convert';

UaaUserResponse uaaUserResponseFromJson(String str) =>
    UaaUserResponse.fromJson(json.decode(str));

String uaaUserResponseToJson(UaaUserResponse data) =>
    json.encode(data.toJson());

class UaaUserResponse {
  bool status;
  dynamic firstName;
  dynamic lastName;
  int id;

  UaaUserResponse({
    this.status,
    this.firstName,
    this.lastName,
    this.id,
  });

  factory UaaUserResponse.fromJson(Map<String, dynamic> json) =>
      UaaUserResponse(
        status: json["status"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "firstName": firstName,
        "lastName": lastName,
        "id": id,
      };
}
