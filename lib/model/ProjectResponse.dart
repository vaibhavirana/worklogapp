// To parse this JSON data, do
//
//     final projectResponse = projectResponseFromJson(jsonString);

import 'dart:convert';

List<ProjectResponse> projectResponseFromJson(String str) => List<ProjectResponse>.from(json.decode(str).map((x) => ProjectResponse.fromJson(x)));

String projectResponseToJson(List<ProjectResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProjectResponse {
  int id;
  String name;
  bool active;
  String description;
  int clientId;
  ClientDto clientDto;

  ProjectResponse({
    this.id,
    this.name,
    this.active,
    this.description,
    this.clientId,
    this.clientDto,
  });

  factory ProjectResponse.fromJson(Map<String, dynamic> json) => ProjectResponse(
    id: json["id"],
    name: json["name"],
    active: json["active"],
    description: json["description"],
    clientId: json["clientId"],
    clientDto: ClientDto.fromJson(json["clientDTO"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "active": active,
    "description": description,
    "clientId": clientId,
    "clientDTO": clientDto.toJson(),
  };
}

class ClientDto {
  int id;
  dynamic login;
  String clientName;
  dynamic password;
  String firstName;
  String lastName;
  String email;
  dynamic imageUrl;
  bool activated;
  dynamic langKey;
  dynamic authorities;
  int phoneNumber;
  bool status;

  ClientDto({
    this.id,
    this.login,
    this.clientName,
    this.password,
    this.firstName,
    this.lastName,
    this.email,
    this.imageUrl,
    this.activated,
    this.langKey,
    this.authorities,
    this.phoneNumber,
    this.status,
  });

  factory ClientDto.fromJson(Map<String, dynamic> json) => ClientDto(
    id: json["id"],
    login: json["login"],
    clientName: json["clientName"],
    password: json["password"],
    firstName: json["firstName"] == null ? null : json["firstName"],
    lastName: json["lastName"] == null ? null : json["lastName"],
    email: json["email"],
    imageUrl: json["imageUrl"],
    activated: json["activated"],
    langKey: json["langKey"],
    authorities: json["authorities"],
    phoneNumber: json["phoneNumber"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "login": login,
    "clientName": clientName,
    "password": password,
    "firstName": firstName == null ? null : firstName,
    "lastName": lastName == null ? null : lastName,
    "email": email,
    "imageUrl": imageUrl,
    "activated": activated,
    "langKey": langKey,
    "authorities": authorities,
    "phoneNumber": phoneNumber,
    "status": status,
  };
}
