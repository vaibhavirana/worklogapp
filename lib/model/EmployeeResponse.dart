// To parse this JSON data, do
//
//     final employeeResponse = employeeResponseFromJson(jsonString);

import 'dart:convert';

EmployeeResponse employeeResponseFromJson(String str) => EmployeeResponse.fromJson(json.decode(str));

String employeeResponseToJson(EmployeeResponse data) => json.encode(data.toJson());

class EmployeeResponse {
  int id;
  String firstName;
  String lastName;
  String fullName;
  String email;
  DateTime birthDate;
  DateTime joinDate;
  int phoneNo;
  String imageUrl;
  String userName;
  List<dynamic> addresses;
  dynamic secondaryPhoneNo;
  bool maritalStatus;
  dynamic anniversaryDate;
  bool status;
  dynamic file;

  EmployeeResponse({
    this.id,
    this.firstName,
    this.lastName,
    this.fullName,
    this.email,
    this.birthDate,
    this.joinDate,
    this.phoneNo,
    this.imageUrl,
    this.userName,
    this.addresses,
    this.secondaryPhoneNo,
    this.maritalStatus,
    this.anniversaryDate,
    this.status,
    this.file,
  });

  factory EmployeeResponse.fromJson(Map<String, dynamic> json) => EmployeeResponse(
    id: json["id"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    fullName: json["fullName"],
    email: json["email"],
    birthDate: DateTime.parse(json["birthDate"]),
    joinDate: DateTime.parse(json["joinDate"]),
    phoneNo: json["phoneNo"],
    imageUrl: json["imageUrl"],
    userName: json["userName"],
    addresses: List<dynamic>.from(json["addresses"].map((x) => x)),
    secondaryPhoneNo: json["secondaryPhoneNo"],
    maritalStatus: json["maritalStatus"],
    anniversaryDate: json["anniversaryDate"],
    status: json["status"],
    file: json["file"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstName": firstName,
    "lastName": lastName,
    "fullName": fullName,
    "email": email,
    "birthDate": birthDate.toIso8601String(),
    "joinDate": joinDate.toIso8601String(),
    "phoneNo": phoneNo,
    "imageUrl": imageUrl,
    "userName": userName,
    "addresses": List<dynamic>.from(addresses.map((x) => x)),
    "secondaryPhoneNo": secondaryPhoneNo,
    "maritalStatus": maritalStatus,
    "anniversaryDate": anniversaryDate,
    "status": status,
    "file": file,
  };
}
