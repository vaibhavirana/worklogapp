import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:portal_app/local/SharedPreferencesHelper.dart';
import 'package:portal_app/model/AccountResponse.dart';
import 'package:portal_app/model/AddWorklogRequest.dart';
import 'package:portal_app/model/ChangePasswordRequest.dart';
import 'package:portal_app/model/EmployeeRequest.dart';
import 'package:portal_app/model/HolidayResponse.dart';
import 'package:portal_app/model/LoginFailureResponse.dart';
import 'package:portal_app/model/LoginRequest.dart';
import 'package:portal_app/model/LoginResponse.dart';
import 'package:portal_app/model/ProjectResponse.dart';
import 'package:portal_app/model/UaaUserResponse.dart';
import 'package:portal_app/model/WorkTypeResponse.dart';
import 'package:portal_app/model/WorklogRequest.dart';
import 'package:portal_app/model/WorklogResponse.dart';

/*https://kernel.thesunflowerlab.com/gateway/portal/api/worklog/
req : {"projectId":64,"worktypeId":3,"date":"2019-10-15","hours":1,"note":"testing whole app after backend changes","uaaUserId":14}

Resp : {"id":12792,"hours":1,"note":"testing whole app after backend changes","status":null,"date":"2019-10-15","projectId":64,"projectName":null,"worktypeId":3,"worktypeName":null,"uaaUserId":14,"userName":"Vaibhavi Rana"}
*/
Future<Object> login(LoginRequest request) async {
  String url = "https://kernel.thesunflowerlab.com/gateway/auth/login";
  print(request.toString());
  final response = await http.post(url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: loginRequestToJson(request));
  print(response.body);
  if (response.statusCode == 200)
    return loginResponseFromJson(response.body);
  else
    return loginFailureResponseFromJson(response.body);
}

Future<bool> forgetPassword(String email) async {
  String url =
      "https://kernel.thesunflowerlab.com/gateway/uaa/api/account/resetGenerated-password/init";
  final response = await http.post(url, body: email);
  if (response.statusCode == 200 ||
      response.statusCode == 201 ||
      response.statusCode == 202)
    return true;
  else
    return false;
}

Future<bool> changePassword(ChangePasswordRequest request) async {
  String url =
      "https://kernel.thesunflowerlab.com/gateway/uaa/api/account/change-password";
  String token;
  await SharedPreferencesHelper.getToken().then((t) {
    token = t;
  });
  print(changePasswordRequestToJson(request));
  final response = await http.post(url,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: token
      },
      body: changePasswordRequestToJson(request));
  print(response.statusCode);
  if (response.statusCode == 200)
    return true;
  else
    return false;
}

Future<AccountResponse> getAccount(String token) async {
  String url = "https://kernel.thesunflowerlab.com/gateway/uaa/api/account";
  final response = await http.get(url, headers: {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.authorizationHeader: token
  });
  print(response.body);
  return accountResponseFromJson(response.body);
}

Future<UaaUserResponse> setUaaUser(String token, int userId) async {
  String url =
      "https://kernel.thesunflowerlab.com/gateway/portal/api/setUaaUser/${userId.toString()}";
  print(userId);
  try {
    /*  var uri = Uri.http('kernel.thesunflowerlab.com',
        '/gateway/portal/api/setUaaUser', {'id': userId.toString()});*/
    final response = await http.get(url, headers: {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.authorizationHeader: token
    });
    print(response.body);
    return uaaUserResponseFromJson(response.body);
  } catch (error) {
    print(error);
  }
}

Future<void> setEmployeeProfile(
    String token, int userId, EmployeeRequest request) async {
  print(request.toString());
  String url =
      "https://kernel.thesunflowerlab.com/gateway/profiling/api/set-employee-profile/${userId.toString()}";
  print(userId);
  try {
    /*  var uri = Uri.http('kernel.thesunflowerlab.com',
        '/gateway/portal/api/setUaaUser', {'id': userId.toString()});*/
    final response = await http.put(url,
        headers: {
          HttpHeaders.acceptHeader: 'application/json',
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: token
        },
        body: employeeRequestToJson(request));
    print(response.body);
    SharedPreferencesHelper.setEmp(response.body);
  } catch (error) {
    print(error);
  }
}

Future<List<HolidayResponse>> getHolidayList() async {
  String token;
  await SharedPreferencesHelper.getToken().then((t) {
    token = t;
  });
  var url = 'https://kernel.thesunflowerlab.com/gateway/portal/api/holidays';

  final response =
      await http.get(url, headers: {HttpHeaders.authorizationHeader: token});
  print("result" + response.body);
  return holidayResponseFromJson(response.body);
}

Future<List<Content>> getWorklog(
    int page, int size, WorklogRequest request) async {
  String token;
  await SharedPreferencesHelper.getToken().then((t) {
    token = t;
  });

  Map<String, String> queryParameters = {
    /* 'page': page,
    'size': size,*/
    'sort': 'date,desc',
    'direction': 'desc'
  };
  var url =
      'https://kernel.thesunflowerlab.com/gateway/portal/api/worklog/filterAllWorklogs/?page=$page&size=$size&sort=date,desc&direction=desc';
  var uri = Uri.https('kernel.thesunflowerlab.com',
      '/gateway/portal/api/worklog/filterAllWorklogs', queryParameters);

  final response = await http.post(url,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: token
      },
      body: worklogRequestToJson(request));
  print("result" + response.body);
  return worklogResponseFromJson(response.body).content;
}

Future<List<ProjectResponse>> getAllActiveProject() async {
  String token;
  await SharedPreferencesHelper.getToken().then((t) {
    token = t;
  });

  var url =
      'https://kernel.thesunflowerlab.com/gateway/portal/api/project/active/all';

  final response = await http.get(url, headers: {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.authorizationHeader: token
  });
  print("result" + response.body);
  return projectResponseFromJson(response.body);
}

Future<List<WorkTypeResponse>> getAllWorkType() async {
  String token;
  await SharedPreferencesHelper.getToken().then((t) {
    token = t;
  });

  var url =
      'https://kernel.thesunflowerlab.com/gateway/portal/api/worktypes/all';

  final response = await http.get(url, headers: {
    HttpHeaders.contentTypeHeader: 'application/json',
    HttpHeaders.authorizationHeader: token
  });
  print("result" + response.body);
  return workTypeResponseFromJson(response.body);
}

Future<int> submitWorkLog(AddWorklogRequest request) async {
  String token;
  await SharedPreferencesHelper.getToken().then((t) {
    token = t;
  });

  var url = 'https://kernel.thesunflowerlab.com/gateway/portal/api/worklog/';

  final response = await http.post(url,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json',
        HttpHeaders.authorizationHeader: token
      },
      body: addWorklogRequestToJson(request));
  print("result" + response.body);
  return response.statusCode;
}
