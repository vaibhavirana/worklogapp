import 'package:flutter/material.dart';
import 'package:portal_app/service/api_services.dart';

import 'holidayCard.dart';
import 'model/HolidayResponse.dart';

class HolidayList extends StatefulWidget {
  @override
  _HolidayListState createState() => new _HolidayListState();
}

class _HolidayListState extends State<HolidayList> {
  @override
  void initState() {
    super.initState();
  }

  final worklogHeader = new Container(
    decoration: BoxDecoration(
      color: Color.fromRGBO(240, 103, 38, 1.0),
    ),
    alignment: Alignment.center,
    height: 58.0,
    child: new Text(
      'Holiday List',
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 103, 38, 1.0),
        title: Container(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/sunflowerLogoWhite.png',
            fit: BoxFit.scaleDown,
          ),
        ),
        leading: IconButton(
          icon: Image.asset(
            'assets/menu.png',
            fit: BoxFit.contain,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
              icon: Image.asset(
                'assets/cancel.png',
                fit: BoxFit.contain,
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
//                  alignment: Alignment.center,
                  child: Image.asset(
                'assets/sunflower_logo.png',
                fit: BoxFit.contain,
              )),
              decoration: BoxDecoration(color: Colors.white12),
            ),
            ListTile(
              title: Text('APP DETAILS'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('HOLIDAY LIST'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('CHANGE PASSWORD'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('LOGOUT'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            worklogHeader,
            Expanded(
              child: FutureBuilder<List<HolidayResponse>>(
                  future: getHolidayList(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) =>
                              holidayCard(holiday: snapshot.data[index]));
                    } else
                      return Center(child: CircularProgressIndicator());
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
