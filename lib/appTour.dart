import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:portal_app/login.dart';

class AppTourPage extends StatefulWidget {
  @override
  _AppTourScreenState createState() => new _AppTourScreenState();
}

class _AppTourScreenState extends State<AppTourPage> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height -24;

    final topImage = new Container(
      alignment: Alignment.center,
      child: new Image.asset('assets/background_image.png'),
    );

    final getStartedButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: ButtonTheme(
        minWidth: width / 2 + width / 3,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26.0),
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute<Null>(builder: (BuildContext context) => LoginPage()));
          },
          padding: EdgeInsets.all(12),
          color: Color.fromRGBO(249, 99, 50, 1.0),
          child: Text('Get Started',
              style: TextStyle(color: Colors.white, height: 1.5)),
        ),
      ),
    );

    final pageController = PageController(initialPage: 0);

    final pageView = PageView(
      controller: pageController,
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/appTour1.png',
                  width: width / 3, height: height / 6),
              Padding(
                  padding: EdgeInsets.only(top: 30.0, left: 30, right: 30),
                  child: Text(
                    'Portal to manage all the worklogs of Sunflower Lab employess',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.normal,
                    ),
                  )),
            ],
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/appTour2.png',
                  width: width / 3, height: height / 6),
              Padding(
                  padding: EdgeInsets.only(top: 30.0, left: 30, right: 30),
                  child: Text(
                    'Add your worklog for the day that you have done',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.normal,
                    ),
                  )),
            ],
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/appTour3.png',
                  width: width / 3, height: height / 6),
              Padding(
                  padding: EdgeInsets.only(top: 30.0, left: 30, right: 30),
                  child: Text(
                    'Use all the useful filters to get all the productive logs',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.normal,
                    ),
                  )),
            ],
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/appTour4.png',
                  width: width / 3, height: height / 6),
              Padding(
                  padding: EdgeInsets.only(top: 30.0, left: 30, right: 30),
                  child: Text(
                    'Get all the statistics of the works and meetings',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.normal,
                    ),
                  )),
            ],
          ),
        ),
      ],
    );

    return new Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 24, width: width),
          //topImage
          Container(
              width: width,
              height: height / 3,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.transparent,
                      width: 1,
                      style: BorderStyle.solid)),
              child: topImage),
          //middle View Pager
          Container(
              alignment: Alignment.center,
              width: width,
              height: height / 3,
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.transparent,
                      style: BorderStyle.solid,
                      width: 1)),
              child: PageIndicatorContainer(
                pageView: pageView,
                length: 4,
                indicatorColor: Colors.grey,
                indicatorSelectorColor: Colors.deepOrange,
                size: 6.0,
                indicatorSpace: 10.0,
                padding: EdgeInsets.only(top: (height / 3) / 3),
              )
          ),
          //Bottom Button
          Container(
            alignment: Alignment.center,
            width: width,
            height: height / 3,
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.transparent, width: 1, style: BorderStyle.solid)),
            child: getStartedButton,
          )
        ],
      ),
    );
  }
}
