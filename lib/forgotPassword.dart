// Copyright 2018-present the Flutter authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:flutter/material.dart';
import 'appTour.dart';
import 'service/api_services.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final _emailController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 100.0),
            Column(
              children: <Widget>[
                Image.asset('assets/sunflower_logo.png'),
              ],
            ),
            SizedBox(height: 60.0),
            TextField(
              controller: _emailController,
              decoration: InputDecoration(
                filled: false,
                labelText: 'Email',
              ),
            ),
            SizedBox(height: 24.0),
            RaisedButton(
              child: Text('Send me my password',
                  style: TextStyle(color: Colors.white, height: 1.5)),
              color: Colors.deepOrange,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(26.0),
              ),
              padding: EdgeInsets.all(12),
              onPressed: () {
                if (_emailController.text.length > 0) {
                  forgetPassword(_emailController.text).then((response) async {
                    if (response) {
                      _emailController.clear();
                      Navigator.pop(context);
                    }
                    else {
                      _scaffoldKey.currentState.showSnackBar(new SnackBar(
                        duration: new Duration(seconds: 4),
                        content: new Row(
                          children: <Widget>[
                            new Text("Entered email is wrong.Please enter valid Email.")
                          ],
                        ),
                      ));
                    }
                  });
                }
                else
                  {
                    _scaffoldKey.currentState.showSnackBar(new SnackBar(
                      duration: new Duration(seconds: 4),
                      content: new Row(
                        children: <Widget>[
                          new Text("Please enter proper email")
                        ],
                      ),
                    ));
                  }
              },
            ),
            SizedBox(height: 6.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  child: Text('Back to Login', textAlign: TextAlign.left),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  child: Text('Need Help?', textAlign: TextAlign.end),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                        builder: (BuildContext context) => AppTourPage()));
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
