// Copyright 2018-present the Flutter authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:flutter/material.dart';
import 'package:portal_app/changePassword.dart';
import 'package:portal_app/local/SharedPreferencesHelper.dart';
import 'package:portal_app/login.dart';
import 'package:portal_app/model/WorklogRequest.dart';
import 'package:portal_app/worklogCard.dart';

import 'addWorklog.dart';
import 'holidayList.dart';
import 'model/WorklogResponse.dart';
import 'service/api_services.dart';
import 'filter.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int uaaUserId = 0;
  WorklogRequest request;
  static var today = new DateTime.now();
  var beforeOneMonth = new DateTime(today.year, today.month - 1, today.day);
  List<Content> contentList = [];
  int _pageNum = 0;
  int _numItemsPage = 10;
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    SharedPreferencesHelper.getUserId().then((id) {
      setState(() {
        uaaUserId = id;
        request = WorklogRequest(
          startDate: beforeOneMonth,
          endDate: today,
          filterBy: "FreeSearch",
          uaaUserId: uaaUserId,
        );
      });
    });

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        setState(() {
          _pageNum += 1;
        });
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final worklogHeader = new Container(
    decoration: BoxDecoration(
      color: Color.fromRGBO(240, 103, 38, 1.0),
    ),
    alignment: Alignment.center,
    height: 58.0,
    child: new Text(
      'Worklog',
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
    ),
  );

  @override
  Widget build(BuildContext context) {
    print("build : $request.toString()");
    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 103, 38, 1.0),
        title: Container(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/sunflowerLogoWhite.png',
            fit: BoxFit.scaleDown,
          ),
        ),
        leading: IconButton(
          icon: Image.asset(
            'assets/menu.png',
            fit: BoxFit.contain,
          ),
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        actions: <Widget>[
          IconButton(
              icon: Image.asset(
                'assets/filter.png',
                fit: BoxFit.contain,
              ),
              onPressed: () {
                _navigateAndFilterWorklog(context);
              })
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Container(
//                  alignment: Alignment.center,
                  child: Image.asset(
                'assets/sunflower_logo.png',
                fit: BoxFit.contain,
              )),
              decoration: BoxDecoration(color: Colors.white12),
            ),
            ListTile(
              title: Text('APP DETAILS'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('HOLIDAY LIST'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute<Null>(
                    builder: (BuildContext context) => HolidayList()));
              },
            ),
            ListTile(
              title: Text('CHANGE PASSWORD'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute<Null>(
                    builder: (BuildContext context) => ChangePassword()));
              },
            ),
            ListTile(
              title: Text('LOGOUT'),
              onTap: () {
                Navigator.pop(context);
                showLogoutDialog();
              },
            ),
          ],
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            worklogHeader,
            Expanded(
              child: FutureBuilder<List<Content>>(
                  future: getWorklogs(request),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                          padding:
                          EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
                          controller: _scrollController,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, index) =>
                              worklogCard(worklog: snapshot.data[index]));
                    } else
                      return Center(child: CircularProgressIndicator());
                  }),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        elevation: 10.0,
        child: Image.asset(
          'assets/addWorklog.png',
          fit: BoxFit.cover,
        ),
        mini: true,
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute<Null>(
              builder: (BuildContext context) => AddWorklogPage()));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  Future<void> showLogoutDialog() {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Logout'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Are you sure to logout?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop();
                SharedPreferencesHelper.clear();
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => LoginPage()),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  _navigateAndFilterWorklog(BuildContext context) async {
    /*Navigator.of(context).push(MaterialPageRoute<Null>(
         builder: (BuildContext context) =>
             FilterPage(uaaUserId: uaaUserId)));*/
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => FilterPage(uaaUserId: uaaUserId)),
    );
    print("home: $result");
    _pageNum=0;
    contentList.clear();
    print("filter : $result");
    getWorklogs(result);
  }

  Future<List<Content>> getWorklogs(WorklogRequest request) async {
    print("PageNum : $_pageNum");
    List<Content> content = await getWorklog( _pageNum,
        _numItemsPage, request);
    if (_pageNum == 0) {
      contentList = content;
    } else {
      contentList.addAll(content);
    }
    return contentList;
  }
}
