import 'package:flutter/material.dart';
import 'package:portal_app/model/HolidayResponse.dart';

class holidayCard extends StatelessWidget {
  final HolidayResponse holiday;

  holidayCard({this.holiday});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 4.0, top: 4.0, right: 4.0, bottom: 4.0),
                    child: Text(
                      '${holiday.holidayName}',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 18.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 4.0, top: 4.0, right: 4.0, bottom: 4.0),
                    child: Text(
                      '${holiday.startDate.month.toString() + '/' + holiday.startDate.day.toString() + '/' + holiday.startDate.year.toString()}',
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
