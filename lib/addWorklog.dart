import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:portal_app/custum/EnsureVisibleWhenFocused.dart';
import 'package:portal_app/model/AddWorklogRequest.dart';
import 'package:toast/toast.dart';

import 'local/SharedPreferencesHelper.dart';
import 'service/api_services.dart';

class AddWorklogPage extends StatefulWidget {
  @override
  _AddWorklogFormState createState() {
    return _AddWorklogFormState();
  }
}

class _AddWorklogFormState extends State<AddWorklogPage>
    with WidgetsBindingObserver {
  List<DropdownMenuItem<String>> _dropDownMenuItems,
      _dropDownMenuWorkTypeItems,
      _dropDownMenuHoursItems;
  String _selectedProj, _selectedWorkType, _selectedHours;
  FocusNode _focusNode = new FocusNode();
  static final TextEditingController _descController =
      new TextEditingController();

  @override
  void initState() {
    super.initState();
    _dropDownMenuItems = getProjectList();
    _dropDownMenuWorkTypeItems = getWorkTypeList();
    _dropDownMenuHoursItems = getHoursList();
    _selectedProj = _dropDownMenuItems[0].value;
    _selectedWorkType = _dropDownMenuWorkTypeItems[0].value;
    _selectedHours = _dropDownMenuHoursItems[0].value;
    _focusNode.addListener(_focusNodeListener);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    _focusNode.removeListener(_focusNodeListener);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {}

  Future<Null> _focusNodeListener() async {
    if (_focusNode.hasFocus) {
      print('TextField got the focus');
    } else {
      print('TextField lost the focus');
    }
  }

  List<DropdownMenuItem<String>> getHoursList() {
    List<DropdownMenuItem<String>> hours = new List();
    hours.add(new DropdownMenuItem(value: '0', child: new Text('Hours')));
    for (int i = 1; i <= 8; i++) {
      hours.add(new DropdownMenuItem(value: '${i}', child: new Text('${i}')));
    }
    return hours;
  }

  List<DropdownMenuItem<String>> getProjectList() {
    List<DropdownMenuItem<String>> projets = new List();
    projets.add(
        new DropdownMenuItem(value: '0', child: new Text('Select Project')));
    getAllActiveProject().then((projetResp) {
      print(projetResp.length);
      for (int i = 0; i < projetResp.length; i++) {
        projets.add(new DropdownMenuItem(
            value: projetResp.elementAt(i).id.toString(),
            child: new Text(projetResp.elementAt(i).name)));
      }
    });
    return projets;
  }

  List<DropdownMenuItem<String>> getWorkTypeList() {
    List<DropdownMenuItem<String>> workType = new List();
    workType.add(
        new DropdownMenuItem(value: '0', child: new Text('Select WorkType')));
    getAllWorkType().then((workTypeResp) {
      print(workTypeResp.length);
      for (int i = 0; i < workTypeResp.length; i++) {
        workType.add(new DropdownMenuItem(
            value: workTypeResp.elementAt(i).id.toString(),
            child: new Text(workTypeResp.elementAt(i).worktypes)));
      }
    });
    return workType;
  }

  String _value = 'Select Date';

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2016),
        lastDate: new DateTime.now());
    if (picked != null) {
      final current =
          new DateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(picked.toString());
      print(current.toString());
      final f = new DateFormat('MM/dd/yyyy').format(current);
      print(f.toString());
      _value = f.toString();
      setState(() => _value);
    }
  }

  @override
  Widget build(BuildContext context) {
    final worklogHeader = Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(240, 103, 38, 1.0),
      ),
      alignment: Alignment.center,
      height: 56.0,
      child: Text(
        'Add Worklog',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 103, 38, 1.0),
        title: Container(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/sunflowerLogoWhite.png',
            fit: BoxFit.scaleDown,
          ),
        ),
        leading: IconButton(
          icon: Image.asset(
            'assets/menu.png',
            fit: BoxFit.contain,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
              icon: Image.asset(
                'assets/cancel.png',
                fit: BoxFit.fill,
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
      body: new Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topCenter,
        child: Column(
          children: <Widget>[
            worklogHeader,
            Form(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 12.0),
                    Container(
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: new BorderRadius.circular(26.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black45,
                              blurRadius: 10.0,
                            ),
                          ]),
                      padding: EdgeInsets.fromLTRB(25, 3, 25, 3),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          icon: Image.asset(
                            'assets/downArrow.png',
                            height: 8.0,
                            color: Colors.black87,
                          ),
                          style:
                              TextStyle(color: Colors.black54, fontSize: 15.0),
                          isExpanded: true,
                          value: _selectedProj,
                          onChanged: (String newValue) {
                            setState(() {
                              _selectedProj = newValue;
                              print(_selectedProj);
                            });
                          },
                          items: _dropDownMenuItems,
                        ),
                      ),
                    ),
                    SizedBox(height: 18.0),
                    Container(
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: new BorderRadius.circular(26.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black45,
                              blurRadius: 10.0,
                            ),
                          ]),
                      padding: EdgeInsets.fromLTRB(25, 3, 25, 3),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          icon: Image.asset(
                            'assets/downArrow.png',
                            height: 8.0,
                            color: Colors.black87,
                          ),
                          style:
                              TextStyle(color: Colors.black54, fontSize: 15.0),
                          isExpanded: true,
                          value: _selectedWorkType,
                          onChanged: (String newValue) {
                            setState(() {
                              _selectedWorkType = newValue;
                            });
                          },
                          items: _dropDownMenuWorkTypeItems,
                        ),
                      ),
                    ),
                    SizedBox(height: 18.0),
                    Container(
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: new BorderRadius.circular(26.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black45,
                              blurRadius: 10.0,
                            ),
                          ]),
                      padding: EdgeInsets.fromLTRB(25, 3, 10, 3),
                      child: GestureDetector(
                        onTap: () {
                          _selectDate();
                        },
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                _value,
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.black54),
                              ),
                              IconButton(
                                  icon: Image.asset(
                                    'assets/calendar.png',
                                    height: 18.0,
                                    width: 18.0,
                                    color: Colors.black87,
                                  ),
                                  onPressed: () {
                                    _selectDate();
                                  })
                            ]),
                      ),
                    ),
                    SizedBox(height: 18.0),
                    Container(
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: new BorderRadius.circular(26.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black45,
                              blurRadius: 10.0,
                            ),
                          ]),
                      padding: EdgeInsets.fromLTRB(25, 3, 10, 3),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          icon: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Image.asset(
                              'assets/time.png',
                              height: 18.0,
                              width: 18.0,
                              color: Colors.black87,
                            ),
                          ),
                          style:
                              TextStyle(color: Colors.black54, fontSize: 15.0),
                          isExpanded: true,
                          value: _selectedHours,
                          onChanged: (String newValue) {
                            setState(() {
                              _selectedHours = newValue;
                            });
                          },
                          items: _dropDownMenuHoursItems,
                        ),
                      ),
                    ),
                    SizedBox(height: 18.0),
                    Container(
                      alignment: Alignment.topLeft,
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius: new BorderRadius.circular(40.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black45,
                              blurRadius: 10.0,
                            ),
                          ]),
                      padding: EdgeInsets.fromLTRB(20, 3, 10, 3),
                      child: EnsureVisibleWhenFocused(
                        focusNode: _focusNode,
                        child: TextFormField(
                          controller: _descController,
                          keyboardType: TextInputType.multiline,
                          maxLines: 3,
                          focusNode: _focusNode,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Description',
                            hasFloatingPlaceholder: false,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 32.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                            width: 150.0,
                            child: RaisedButton(
                              child: Text('Cancel',
                                  style: TextStyle(
                                      color: Colors.deepOrange, height: 1.5)),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(26.0),
                              ),
                              padding: EdgeInsets.all(12),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            )),
                        SizedBox(
                            width: 150.0,
                            child: RaisedButton(
                              child: Text('Add',
                                  style: TextStyle(
                                      color: Colors.white, height: 1.5)),
                              color: Colors.deepOrange,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(26.0),
                              ),
                              padding: EdgeInsets.all(12),
                              onPressed: () async {
                                // add
                                AddWorklogRequest request =
                                    new AddWorklogRequest();

                                await SharedPreferencesHelper.getUserId().then((id) {
                                  request.uaaUserId = id;
                                });
                                request.note = _descController.text;
                                final current =
                                new DateFormat("MM/dd/yyyy").parse(_value);
                                final f = new DateFormat('yyyy-MM-dd').format(current);
                                request.date =f ;
                                print(f);
                                print(_selectedHours);
                                print(_selectedProj);
                                print(_selectedWorkType);
                                request.hours = int.parse(_selectedHours);
                                request.projectId = int.parse(_selectedProj);
                                request.worktypeId = int.parse(_selectedWorkType);
                                print(request);
                                int result = await submitWorkLog(request);
                                if (result == 200) {
                                  Navigator.pop(context);
                                } else {
                                  Toast.show("Something went wrong. Please try again later!", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
                                }
                              },
                            )),
                      ],
                    ),
                    SizedBox(height: 24.0),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
