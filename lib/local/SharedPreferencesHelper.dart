import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  ///
  static final String _token = "token";
  static final String _userId = "userId";
  static final String _username = "userName";
  static final String _userEmail = "userEmail";
  static final String _emp = "employee";


  static Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_token) ?? null;
  }

  static Future<bool> setToken(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(_token, value);
  }

  static Future<int> getUserId() async  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_userId) ?? 0;
  }

  static Future<bool> setUserId(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setInt(_userId, id);
  }

  static Future<String> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_username) ?? null;
  }

  static Future<bool> setUserName(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(_username, value);
  }

  static Future<String> getUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_userEmail) ?? null;
  }

  static Future<bool> setUserEmail(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(_userEmail, value);
  }
  static Future<String> getEmp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_emp) ?? null;
  }

  static Future<bool> setEmp(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(_emp, value);
  }
  static Future<bool> clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }
}
