import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:portal_app/model/WorklogRequest.dart';
import 'package:portal_app/service/api_services.dart';
import 'package:toast/toast.dart';

class FilterPage extends StatefulWidget {
  final int uaaUserId;

  @override
  _FilterFormState createState() {
    return _FilterFormState(uaaUserId);
  }

  FilterPage({Key key, @required this.uaaUserId}) : super(key: key);
}

class _FilterFormState extends State<FilterPage> {
  int userId;
  List<DropdownMenuItem<String>> _dropDownProjectList,
      _dropDownMenuWorkTypeItems;
  String _selectedProj, _selectedWorkType;

  _FilterFormState(int uaaUserId) {
    this.userId = uaaUserId;
    print(userId);
  }

  @override
  void initState() {
    _dropDownProjectList = getProjectList();
    _dropDownMenuWorkTypeItems = getWorkTypeList();
    _selectedProj = _dropDownProjectList[0].value;
    _selectedWorkType = _dropDownMenuWorkTypeItems[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getProjectList() {
    List<DropdownMenuItem<String>> projects = new List();
    projects.add(
        new DropdownMenuItem(value: '0', child: new Text('Select Project')));
    getAllActiveProject().then((projectResp) {
      for (int i = 0; i < projectResp.length; i++) {
        projects.add(new DropdownMenuItem(
            value: projectResp.elementAt(i).name,
            child: new Text(projectResp.elementAt(i).name)));
      }
    });
    return projects;
  }

  List<DropdownMenuItem<String>> getWorkTypeList() {
    List<DropdownMenuItem<String>> workType = new List();
    workType.add(
        new DropdownMenuItem(value: '0', child: new Text('Select WorkType')));
    getAllWorkType().then((workTypeResp) {
      for (int i = 0; i < workTypeResp.length; i++) {
        workType.add(new DropdownMenuItem(
            value: workTypeResp.elementAt(i).worktypes,
            child: new Text(workTypeResp.elementAt(i).worktypes)));
      }
    });
    return workType;
  }

  static var today = new DateTime.now();
  var beforeOneMonth = new DateTime(today.year, today.month - 1, today.day);

  String _startDateValue = 'Start Date';

  Future _selectStartDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2016),
        lastDate: new DateTime.now());
    if (picked != null) {
      final current =
          new DateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(picked.toString());
      print(current.toString());
      final f = new DateFormat('MM/dd/yyyy').format(current);
      print(f.toString());
      _startDateValue = f.toString();
      setState(() => _startDateValue);
    }
  }

  String _endDateValue = 'End Date';

  Future _selectEndDate() async {
    if (_startDateValue == 'Start Date') {
      Toast.show("please select start date", context, duration: 1);
    } else {
      DateTime picked = await showDatePicker(
          context: context,
          initialDate: new DateTime.now(),
          firstDate: new DateFormat('MM/dd/yyyy').parse(_startDateValue),
          lastDate: new DateTime.now());
      if (picked != null) {
        final current =
            new DateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(picked.toString());
        print(current.toString());
        final f = new DateFormat('MM/dd/yyyy').format(current);
        print(f.toString());
        _endDateValue = f.toString();
        setState(() => _endDateValue);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final worklogHeader = Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(240, 103, 38, 1.0),
      ),
      alignment: Alignment.center,
      height: 58.0,
      child: Text(
        'Filter',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(240, 103, 38, 1.0),
        title: Container(
          alignment: Alignment.center,
          child: Image.asset(
            'assets/sunflowerLogoWhite.png',
            fit: BoxFit.scaleDown,
          ),
        ),
        leading: IconButton(
          icon: Image.asset(
            'assets/menu.png',
            fit: BoxFit.contain,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          IconButton(
              icon: Image.asset(
                'assets/cancel.png',
                fit: BoxFit.contain,
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
      body: new Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
        child: Column(
          children: <Widget>[
            worklogHeader,
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(height: 24.0),
                  Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(26.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black45,
                            blurRadius: 10.0,
                          ),
                        ]),
                    padding: EdgeInsets.fromLTRB(25, 3, 25, 3),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        icon: Image.asset(
                          'assets/downArrow.png',
                          height: 8.0,
                          color: Colors.black87,
                        ),
                        style: TextStyle(color: Colors.black54, fontSize: 15.0),
                        isExpanded: true,
                        value: _selectedProj,
                        onChanged: (String newValue) {
                          setState(() {
                            _selectedProj = newValue;
                            print(_selectedProj);
                          });
                        },
                        items: _dropDownProjectList,
                      ),
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(26.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black45,
                            blurRadius: 10.0,
                          ),
                        ]),
                    padding: EdgeInsets.fromLTRB(25, 3, 25, 3),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        icon: Image.asset(
                          'assets/downArrow.png',
                          height: 8.0,
                          color: Colors.black87,
                        ),
                        style: TextStyle(color: Colors.black54, fontSize: 15.0),
                        isExpanded: true,
                        value: _selectedWorkType,
                        onChanged: (String newValue) {
                          setState(() {
                            _selectedWorkType = newValue;
                          });
                        },
                        items: _dropDownMenuWorkTypeItems,
                      ),
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(26.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black45,
                            blurRadius: 10.0,
                          ),
                        ]),
                    padding: EdgeInsets.fromLTRB(25, 3, 10, 3),
                    child: GestureDetector(
                      onTap: () {
                        _selectStartDate();
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              _startDateValue,
                              style: TextStyle(
                                  fontSize: 16.0, color: Colors.black54),
                            ),
                            IconButton(
                                icon: Image.asset(
                                  'assets/calendar.png',
                                  height: 18.0,
                                  width: 18.0,
                                  color: Colors.black87,
                                ),
                                onPressed: () {
                                  _selectStartDate();
                                })
                          ]),
                    ),
                  ),
                  SizedBox(height: 24.0),
                  Container(
                    decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(26.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.black45,
                            blurRadius: 10.0,
                          ),
                        ]),
                    padding: EdgeInsets.fromLTRB(25, 3, 10, 3),
                    child: GestureDetector(
                      onTap: () {
                        _selectEndDate();
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              _endDateValue,
                              style: TextStyle(
                                  fontSize: 16.0, color: Colors.black54),
                            ),
                            IconButton(
                                icon: Image.asset(
                                  'assets/calendar.png',
                                  height: 18.0,
                                  width: 18.0,
                                  color: Colors.black87,
                                ),
                                onPressed: () {
                                  _selectEndDate();
                                })
                          ]),
                    ),
                  ),
                  SizedBox(height: 28.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                          width: 150.0,
                          child: RaisedButton(
                            child: Text('Reset',
                                style: TextStyle(
                                    color: Colors.deepOrange, height: 1.5)),
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(26.0),
                            ),
                            padding: EdgeInsets.all(12),
                            onPressed: () {
                              setState(() {
                                if (_startDateValue != 'Start Date') {
                                  _startDateValue = 'Start Date';
                                }
                                if (_endDateValue != 'End Date') {
                                  _endDateValue = 'End Date';
                                }
                                _selectedProj = _dropDownProjectList[0].value;
                                _selectedWorkType =
                                    _dropDownMenuWorkTypeItems[0].value;
                              });
                            },
                          )),
                      SizedBox(
                          width: 150.0,
                          child: RaisedButton(
                            child: Text('Filter',
                                style: TextStyle(
                                    color: Colors.white, height: 1.5)),
                            color: Colors.deepOrange,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(26.0),
                            ),
                            padding: EdgeInsets.all(12),
                            onPressed: () {
                              WorklogRequest request = new WorklogRequest();
                              if (_startDateValue != 'Start Date') {
                                final startDateCurrent =
                                    new DateFormat("MM/dd/yyyy")
                                        .parse(_startDateValue);
                                request.startDate = startDateCurrent;
                              } else {
                                request.startDate = beforeOneMonth;
                              }

                              if (_endDateValue != 'End Date') {
                                final endDateCurrent =
                                    new DateFormat("MM/dd/yyyy")
                                        .parse(_endDateValue);
                                request.endDate = endDateCurrent;
                              } else {
                                request.endDate = today;
                              }
                              request.uaaUserId = userId;
                              request.filterBy = "FreeSearch";
                              request.workType = _selectedWorkType != '0'
                                  ? _selectedWorkType
                                  : null;
                              request.projectName =
                                  _selectedProj != '0' ? _selectedProj : null;
                              print(request.toString());
                              Navigator.pop(context, request);
                            },
                          )),
                    ],
                  ),
                  SizedBox(height: 24.0),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
