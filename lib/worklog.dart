class Worklog{
  final int id;
  final double hours;
  final String note;
  final String status;
  final DateTime date;
  final int projectId;
  final String projectName;
  final int worktypeId;
  final String worktypeName;
  final int uaaUserId;
  final int userId;
  final String userName;

  Worklog(this.id, this.hours, this.note, this.status, this.date,
      this.projectId, this.projectName, this.worktypeId, this.worktypeName,
      this.uaaUserId, this.userId, this.userName);

}




/*
* {
  "id" : 1915,
  "hours" : 8,
  "note" : "Created New Roadmap Project with existing work",
  "status" : null,
  "date" : "2019-02-04",
  "projectId" : 43,
  "projectName" : "Roadmap",
  "worktypeId" : 2,
  "worktypeName" : "Development",
  "uaaUserId" : 14,
  "userId" : 27,
  "userName" : "purvik.rana"
}
* */