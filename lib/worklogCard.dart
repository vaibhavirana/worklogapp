import 'package:flutter/material.dart';

import 'model/WorklogResponse.dart';

class worklogCard extends StatelessWidget {
  final Content worklog;

  worklogCard({this.worklog});

  String _getMonth(int month) {
    switch (month) {
      case 1:
        return "JAN";
      case 2:
        return "FEB";
      case 3:
        return "MAR";
      case 4:
        return "APR";
      case 5:
        return "MAY";
      case 6:
        return "JUN";
      case 7:
        return "JUL";
      case 8:
        return "AUG";
      case 9:
        return "SEP";
      case 10:
        return "OCT";
      case 11:
        return "NOV";
      case 12:
        return "DEC";
    }
  }

  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 5.0,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            //DatePart
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  _getMonth(worklog.date.month),
                  style: TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.w300),
                ),
                Text(
                  '${worklog.date.day.toString().toUpperCase()}',
                  style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(240, 103, 38, 1.0)),
                ),
              ],
            ),
            //MiddlePart
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  //Title

                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, top: 2.0, right: 8.0, bottom: 2.0),
                    child: Text(
                      '${worklog.projectName}',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 18.0),
                    ),
                  ),
                  //WorkType
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, top: 2.0, right: 8.0, bottom: 2.0),                            child: Text(
                    '${worklog.worktypeName}',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        color: Color.fromRGBO(240, 103, 38, 1.0)),
                  ),
                  ),
                  //Description
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, top: 2.0, right: 8.0, bottom: 4.0),
                    child: Text(
                      '${worklog.note}',
                      style: TextStyle(fontSize: 14.0,
                          fontWeight: FontWeight.w400,
                          color: Colors.blueGrey),
                    ),
                  ),
                ],
              ),
            ),
            //HourPart
            Container(
              width: 32.0,
              height: 32.0,
              padding: EdgeInsets.all(4.0),
              margin: EdgeInsets.only(right: 16.0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.deepOrange,
              ),
              child: Text(
                '${worklog.hours}',
                style: TextStyle(
                    fontWeight: FontWeight.w400, color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
