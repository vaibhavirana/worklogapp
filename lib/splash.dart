// Copyright 2018-present the Flutter authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:portal_app/home.dart';
import 'package:portal_app/local/SharedPreferencesHelper.dart';
import 'package:portal_app/welcome.dart';

// TODO: Convert ShrineApp to stateful widget (104)
class SplashPage extends StatefulWidget {

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    SharedPreferencesHelper.getToken().then((token){
      new Timer(const Duration(seconds: 2), () {
        if (token != null) {
          Navigator.of(context).push(MaterialPageRoute<Null>(
              builder: (BuildContext context) => HomePage()));
        } else {
          Navigator.of(context).push(MaterialPageRoute<Null>(
              builder: (BuildContext context) => WelcomePage()));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(height: 120.0),
          Image.asset('assets/sunflower_logo.png'),
          Image.asset('assets/splash_image.png'),
        ],
      )),
    );
  }
}

// TODO: Build a Shrine Theme (103)

// TODO: Build a Shrine Text Theme (103)
