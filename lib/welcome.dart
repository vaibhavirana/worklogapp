import 'package:flutter/material.dart';
import 'package:portal_app/login.dart';
import 'package:portal_app/appTour.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => new _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    final topImageStack = Stack(
      children: <Widget>[
        Container(
          alignment: Alignment.topCenter,
          width: width,
          height: height / 2,
          child: new Image.asset('assets/background_image.png'),
        ),
        Container(
          alignment: Alignment.center,
          width: width,
          height: height / 2 + height / 6,
          child: new Image.asset('assets/welcome_image.png'),
        ),
      ],
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: ButtonTheme(
        minWidth: width / 2 + width / 3,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26.0),
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute<Null>(builder: (BuildContext context) => LoginPage()));
          },
          padding: EdgeInsets.all(12),
          color: Color.fromRGBO(249, 99, 50, 1.0),
          child: Text('Log In',
              style: TextStyle(color: Colors.white, height: 1.5)),
        ),
      ),
    );

    final appTourButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: ButtonTheme(
        minWidth: width / 2 + width / 3,
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26.0),
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute<Null>(builder: (BuildContext context) => AppTourPage()));
          },
          padding: EdgeInsets.all(12),
          color: Color.fromRGBO(249, 99, 50, 1.0),
          child: Text('Take a tour',
              style: TextStyle(color: Colors.white, height: 1.5)),
        ),
      ),
    );

    return Scaffold(
      body: new Container(
        padding: EdgeInsets.all(0.0),
        alignment: Alignment.center,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 24, width: width),
            topImageStack,
            appTourButton,
            loginButton,
          ],
        ),
      ),
    );
  }
}
