// Copyright 2018-present the Flutter authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:flutter/material.dart';
import 'package:portal_app/local/SharedPreferencesHelper.dart';
import 'package:portal_app/model/EmployeeRequest.dart';
import 'package:portal_app/model/LoginFailureResponse.dart';
import 'package:portal_app/model/LoginRequest.dart';
import 'package:portal_app/model/LoginResponse.dart';
import 'appTour.dart';
import 'forgotPassword.dart';
import 'home.dart';
import 'service/api_services.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 100.0),
            Column(
              children: <Widget>[
                Image.asset('assets/sunflower_logo.png'),
              ],
            ),
            SizedBox(height: 60.0),
            TextField(
              controller: _usernameController,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                filled: false,
                labelText: 'Username',
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              controller: _passwordController,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                filled: false,
                labelText: 'Password',
              ),
              obscureText: true,
            ),
            SizedBox(height: 24.0),
            RaisedButton(
              child: Text('Login',
                  style: TextStyle(color: Colors.white, height: 1.5)),
              color: Colors.deepOrange,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(26.0),
              ),
              padding: EdgeInsets.all(12),
              onPressed: () {
                if (_usernameController.text.length > 0 &&
                    _passwordController.text.length > 0) {
                  _scaffoldKey.currentState.showSnackBar(new SnackBar(
                    duration: new Duration(seconds: 4),
                    content: new Row(
                      children: <Widget>[
                        new CircularProgressIndicator(),
                        new Text("  Signing-In...")
                      ],
                    ),
                  ));
                  print(_usernameController.text);
                  print(_passwordController.text);

                  LoginRequest request = LoginRequest(
                      username: _usernameController.text,
                      password: _passwordController.text,
                      rememberMe: false);
                  login(request).then((response) async {
                    if (response is LoginResponse) {
                      await SharedPreferencesHelper.setToken(
                          response.tokenType + " " + response.accessToken);
                      await SharedPreferencesHelper.getToken()
                          .then((token) async {
                        print(token);
                        await getAccount(token).then((acctResponse) async {
                          await SharedPreferencesHelper.setUserName(
                              acctResponse.login);
                          await SharedPreferencesHelper.setUserEmail(
                              acctResponse.email);
                          await setUaaUser(token, acctResponse.id)
                              .then((uaaUserResponse) async {
                            await SharedPreferencesHelper.setUserId(
                                uaaUserResponse.id);
                            setEmployeeProfile(token, acctResponse.id, new EmployeeRequest(email:acctResponse.email, userName: acctResponse.login));
                            _usernameController.clear();
                            _passwordController.clear();
                            Navigator.of(context).push(MaterialPageRoute<Null>(
                                builder: (BuildContext context) => HomePage()));
                          });
                        });
                      });
                    } else if (response is LoginFailureResponse) {
                      _scaffoldKey.currentState.showSnackBar(new SnackBar(
                        duration: new Duration(seconds: 4),
                        content: new Row(
                          children: <Widget>[new Text(response.title)],
                        ),
                      ));
                    }
                  });
                } else {
                  _scaffoldKey.currentState.showSnackBar(new SnackBar(
                    duration: new Duration(seconds: 4),
                    content: new Row(
                      children: <Widget>[
                        new Text("Please enter username/password")
                      ],
                    ),
                  ));
                }
              },
            ),
            SizedBox(height: 6.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  child: Text('Forget Password?', textAlign: TextAlign.left),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                        builder: (BuildContext context) =>
                            ForgotPasswordPage()));
                  },
                ),
                FlatButton(
                  child: Text('Need Help?', textAlign: TextAlign.end),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute<Null>(
                        builder: (BuildContext context) => AppTourPage()));
                    //Navigator.pop(context);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
